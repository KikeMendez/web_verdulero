<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Cliente extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -  
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in 
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	public function __construct()
	{
		parent::__construct();
		$this->load->helper('form');
		
	}

	public function index()
	{
		$data = array(
			"main_content"  =>  "Login", // main content of the page
			"header"		=>  "includes/HeaderCliente", 
			"nav"			=>	'includes/nav' // menu navigation

			);
		$this->load->view('includes/Templates',$data);
	}

		public function set_session()
	{
		/*
		 *  USER INFORMATION FROM CLIENTE.JS -> STORED IN SESSION VARIABLE	
		 */
		$id 			= $this->input->post('id');	
		$name 			= $this->input->post('name');
		$mail 			= $this->input->post('mail');
		$telefono 		= $this->input->post('phone');
		$apiKey			= $this->input->post('apiKey');
		$nivel_listados = $this->input->post('nivel_listado');
		
		
		$user_session_data = array(
			'id'	=>$id,
			'name'  	=> $name,
			'mail'   => $mail,
			'telefono'=>$telefono,
			'apiKey' => $apiKey,
			'nivel_listados'=>  $nivel_listados,
			'logged_in' => 	TRUE
			);
		$this->session->set_userdata($user_session_data);

		
		
	}//end session

		public function log_out()
	{	
		//$this->session->unset_userdata();
		$this->session->sess_destroy();
		redirect(base_url());
	}

		public function terminar_pedido()
	{	
		$this->session->unset_userdata('id_pedido');
		$this->session->set_userdata('id_pedido',null);
		//$this->session->sess_destroy();
		redirect(base_url('pedido'));
	}
}

