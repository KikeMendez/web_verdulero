<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Pedido extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -  
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in 
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	public function __construct()
	{
		parent::__construct();
		$this->load->helper('form');
		
	}

	public function index()
	{
		$data = array(
			'main_content'  =>  'Pedido', // main content of the page
			'header'		=>  'includes/HeaderPedido', 
			'nav'			=>	'includes/Menu_nav' // menu navigation
			);
		$this->load->view('includes/Templates',$data);
	}

	public function session_id_pedido(){
		$id_pedido 	= $this->input->post('id_pedido');
		$this->session->set_userdata('id_pedido',$id_pedido);
	}
}

