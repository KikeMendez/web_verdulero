<!-- CHECK IF EXITS A SESSION AND IF THE SESSION NIVEL LISTADOS == ADMIN(4)-->
<?php if ($this->session->has_userdata('logged_in') && ($this->session->userdata('nivel_listados') == 4)) :?>	
<?php else : redirect(base_url("cliente"))  ?>
<?php endif;?>

</br>
     <div class="panel panel-default" id="panel">   
        <div class="page-header">
        	<h3 id='adminPanel'>Panel de administracion</h3>
        	<h4 id='SubTitle' class="admin_sub_title"></h4>
        	<h3 id='message' class='' style="display:none; color:green;"></h3>
        </div>
      <div class="panel-body">
          	
        <!-- agregar producto -->          
        <div id="Producto"  style="display:none;">
        	 <!--Form Productos-->
			<form name="frmAddProduct" id="frmAddProduct" method="post" action="" class="form-horizontal">
	        
	        <div class="form-group">
	            <label for="nombre_producto" class="col-sm-4 control-label">Producto:</label>
	               <div class="col-sm-4">
	                 <div class="input-group">
	                    <span class="input-group-addon"><span class="glyphicon glyphicon-user"></span></span>
	                      <input type="text" class="form-control" id="nombre_producto" name="ProductoNombre" placeholder="Anana" value="" autofocus >                  
	                  </div>
	                </div>
	        </div>
	                      

	         <div class="form-group">
	            <label for="unidades_disponibles" class="col-sm-4  control-label">Unidades disponibles:</label>
	               <div class="col-sm-4">
	                  <div class="input-group">
	                    <span class="input-group-addon"><span class="glyphicon glyphicon-lock"></span></span>
	                      <select class="form-control" name='unidades' id='unidades_disponibles'>	  
		                      
		                      	<option value=''>Elija una opcion</option>
		               
		                      	<option value='kilo'>kilo</option>
		                      	<option value='bolsa'>bolsa</option>
		                      	<option value='cajon'>cajon</option>
		                      	<option value='plancha'>plancha</option>
		                      	<option value='planchon'>planchon</option>
		                      	<option value='unidad'>unidad</option>
		                                            		

		                      <optgroup label="2 combinaciones">
		                      	<option value='kilo,bolsa'>kilo,bolsa</option>
		                      	<option value='kilo,cajon'>kilo,cajon</option>
		                      	<option value='kilo,plancha'>kilo,plancha</option>
		                      	<option value='kilo,planchon'>kilo,planchon</option>
		                      </optgroup>
		                      	
		                      <optgroup label="3 combinaciones">
		                      	<option value='unidad,kilo,bolsa'>unidad,kilo,bolsa</option>
		                      	<option value='unidad,kilo,cajon'>unidad,kilo,cajon</option>
		                      	<option value='unidad,kilo,plancha'>unidad,kilo,plancha</option>
		                      	<option value='unidad,kilo,planchon'>unidad,kilo,planchon</option>
		                      </optgroup>
	                      </select>
	                  </div>                         
	                </div>
	        </div>

	        <div class="form-group">
	            <label for="estado" class="col-sm-4  control-label">Estado:</label>
	               <div class="col-sm-4">
	                  <div class="input-group">
	                    <span class="input-group-addon"><span class="glyphicon glyphicon-lock"></span></span>
	                      <select class="form-control" name='estado' id="estado">
	                      	<option value=''>Elija</option>
	                      	<option value='1'>Activo</option>
	                      	<option value='0'>Inactivo</option>
	                      </select>
	                  </div>                         
	                </div>
	        </div>

	        <div class="form-group">
	            <label for="nivel_listado" class="col-sm-4  control-label">Nivel Listado:</label>
	               <div class="col-sm-4">
	                  <div class="input-group">
	                    <span class="input-group-addon"><span class="glyphicon glyphicon-lock"></span></span>
	                      <select class="form-control" name='nivel_listado' id='nivel_listado'>
	                      	<option value=''>Elija</option>
	                      	<option value='1'>Puede ver visto por todos</option>
	                      	<option value='2'>Puede ser visto por cliente nivel 2</option>
	                      	<option value='3'>Puede ser visto por cliente nivel 3</option>
	                      	<option value='4'>Puede ser visto solo por el administrador</option>
	                      </select>
	                  </div>                         
	                </div>
	        </div>


          	<div class="form-group">
          		<div class="col-sm-offset-4 col-sm-6">
          		    <input type="submit" class="btn btn-success" name="AgregarProducto"  value="Agregar producto"/> 
          		</div>
       	   </div> 
		</form>
       </div>

<!--********************AGREGARCLIENTE****************************************************--> 
    	<div id="Cliente"  style="display:none;">
        	<form name="AddClient" id="AddClient" method="POST" action="" class="form-horizontal">
	        
			<div class="form-group">
	            <label for="email" class="col-sm-4  control-label">Correo electronico:</label>
	                <div class="col-sm-4">
	                  <div class="input-group">
	                    <span class="input-group-addon"><span class="glyphicon glyphicon-envelope"></span></span>
	                      <input type="text" class="form-control" id="email" name="mail" placeholder="Verdulero@example.com" autofocus >
	                  </div>                         
	                </div>
	        </div>  

	        <div class="form-group">
	            <label for="name" class="col-sm-4 control-label">Local:</label>
	               <div class="col-sm-4">
	                 <div class="input-group">
	                    <span class="input-group-addon"><span class="glyphicon glyphicon-user"></span></span>
	                      <input type="text" class="form-control" id="name" name="nombre" placeholder="Juan Verdura">                  
	                  </div>
	                </div>
	        </div>   

	         <div class="form-group">
	            <label for="phone" class="col-sm-4  control-label">Telefono:</label>
	                <div class="col-sm-4">
	                  <div class="input-group">
	                    <span class="input-group-addon"><span class="glyphicon glyphicon-phone-alt"></span></span>
	                      <input type="text" class="form-control" id="phone" name="telefono" placeholder=" 555 555 555">
	                  </div>                         
	                </div>
	        </div>

		<div class="form-group">
	            <label for="Pass" class="col-sm-4  control-label">Password:</label>
	                <div class="col-sm-4">
	                  <div class="input-group">
	                    <span class="input-group-addon"><span class="glyphicon glyphicon-lock"></span></span>
	                      <input type="password" class="form-control" id="Pass" name="password" placeholder="Password">
	                  </div>                         
	                </div>
	        </div>

	         <div class="form-group">
	            <label for="nivel_listado" class="col-sm-4  control-label">Nivel Listado:</label>
	               <div class="col-sm-4">
	                  <div class="input-group">
	                    <span class="input-group-addon"><span class="glyphicon glyphicon-lock"></span></span>
	                      <select class="form-control" name='nivel' id='nivel_listado'>
	                      	<option value=''>Elija</option>
	                      	<option value='1'>Usuario nivel  1</option>
	                      	<option value='2'>Usuario nivel 2</option>
	                      	<option value='3'>Usuario nivel 3</option>
	                      	<option value='4'>Nivel administrador</option>
	                      </select>
	                  </div>                         
	                </div>
	        </div>

          	
          	<div class="form-group">
          		<div class="col-sm-offset-4 col-sm-6">
          		    <input type="submit" class="btn btn-success" name="insertar" id="Submit_User" value="Insertar nuevo usuario"/> 
          		</div>
       	   </div> 

		</form>
        </div>
        <!--DISPLAY ALL ORDERS-->
        <div id="Pedido">
          			
        </div>

        <!--DISPLAY ORDER DETAIL-->
        <div id="Detalle">
        </div>


        <!--Welcome admin page-->
        <div id="Welcome">      
        	<div>
        		<h4>Hola <?= $this->session->userdata('name');?> este es el panel de administracion.</h4>
        	</div>
        	<div>
        		<h5>Desde aqui usted podra manejar su aplicacion.</h5></br>
        		<p>Las opciones disponibles estan en el menu de navegacion</p>
        		</div>
        	</div>
        <div>
		<!--end div panel-->
      </div>
    </div>

                
  
