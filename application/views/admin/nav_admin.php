<nav class="navbar navbar-inverse navbar-fixed-top">
  <div class="container-fluid">
    
    <div class="navbar-header">
       <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
      <a class="navbar-brand" href="<?= base_url('Admin');?>"><?= ucfirst($this->session->userdata('name'));?></a>
    </div>
    
 <div id="navbar" class="collapse navbar-collapse">
      <ul class="nav navbar-nav navbar-right" >           
            <li ><a id='MostrarPedido'><span class="glyphicon glyphicon-list"></span> Mis pedidos</a></li>
            <li><a id='AgregarProducto'><span class="glyphicon glyphicon-pencil"></span>Agregar Producto</a></li>
                
            <li ><a  id='AgregarCliente'><span class="glyphicon glyphicon-plus"></span> Agregar Cliente</a></li>
            <li><a href="<?= base_url('Cliente/log_out');?>"><span class="glyphicon glyphicon-log-out"></span>Cerrar Session</a></li> 
            <li><a href=""></a></li>
        </ul>
    </div>
  </div>
</nav>
</br></br>