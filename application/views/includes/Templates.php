 <!-- This is the html header-->
<?php $this->load->view($header); ?>

<!-- This is the navegation menu-->
<?php $this->load->view($nav); ?>

<!-- This is the dinamic content-->
<?php $this->load->view($main_content); ?>

<!-- This is the footer-->
<?php $this->load->view('includes/Footer'); ?>