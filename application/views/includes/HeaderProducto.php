<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="<?php echo base_url('assets/images/pinguin.jpeg');?>">

    <title>Web | Verdulero</title>

    <!-- From assets CSS -->
    <link rel="stylesheet" href="<?php echo base_url("assets/css/bootstrap.min.css");?>" media="screen">

    <!-- From assets own styles -->
    <link rel="stylesheet" href="<?php echo base_url("assets/css/styles.css");?>" media="screen">

    <!-- From assets CSS -->
    <link rel="stylesheet" href="<?php echo base_url("assets/css/bootstrap-theme.min.css");?>">
   
     <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js"></script> 
     <script src="assets/js/producto.js"></script>
     <script src="assets/js/urls.js"></script>
     <script src="https://cdn.jsdelivr.net/jquery.validation/1.15.0/jquery.validate.min.js"></script>
     <script src="https://cdn.jsdelivr.net/jquery.validation/1.15.0/additional-methods.min.js"></script>
  


 </head>
<body>