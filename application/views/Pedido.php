<?php if ($this->session->has_userdata('logged_in')) :?>
<?php else : redirect(base_url("cliente"))  ?>
<?php endif;?>
<?php
  $idCliente = $this->session->userdata('id');
  $idPedido = $this->session->userdata('id_pedido');
  $user_name = $this->session->userdata('name');
  $client_level =  $this->session->userdata('nivel_listados');
?>
</br>
<!--TABLE -->
<div class="container-fluid">
<div class="row">
  <div class="col-md-8">
    <div class="panel panel-default" id="tableOrder">
        <div class="table-responsive"  style="margin:auto;">          
          <table class="table">
            <thead>
              <tr>
                <th>Producto</th>
                <th>Cantidad</th>
                <th>Unidad</th>
                <th>Fecha entrega</th>
                <th></th>
                
                <th>Accion</th>   
              </tr>
            </thead>
            <!--TABLE BODY -->
            <tbody id="displayOrder">

            </tbody>
          </table>
        </div>
       </div> 

     <div class="panel panel-default" id="panel">   
      <div class="panel-body">
        <div class="page-header" >
          <h3 id="mensaje"></h3>
        </div>
           <!-- fecha carga el textarea y el btn NewOrder-->
           <div id="fecha" class="row">
                 <div class="col-xs-6 col-md-6">
                     <label for='fecha'>Cuando quiere que le entreguemos su pedido?</label>
                      <div class="form-group">
                           <div class="input-group date">
                                <input type="date" class="form-control" id="fechaEntrega">
                                <span class="input-group-addon">
                                <span class="glyphicon glyphicon-calendar"></span></span>
                            </div>
                      </div>
                    </div>
            </div>
                   
                     <!-- button new order-->
            <div id="btnNewOrder" style="margin-left: 4px;" >
              
            </div>
                 
           
       <form name="frmPedido" method="POST" action="" id="frmPedidos" class="form-horizontal">
        <div class="form-group" id="frmgroup">
            <label for="Item" class="col-sm-1 control-label">Producto</label>
               <div class="col-sm-2">
                 <div class="input-group" id="SelectItem">
                      <!--SELECT->CONTENT LOADED BY JQUERY--> 
                      <select class="form-control" id="selectP" name="producto">

                      </select>                 
                  </div>
                </div>
            
                <label for="unidad" class="col-sm-1 control-label">Unidad</label>
               <div class="col-sm-2">
                 <div class="input-group">
                      <select class="form-control" id="selectU" name="unidad">
                      
                      </select>                  
                  </div>
                </div>


               <label for="Cantidad" class="col-sm-1 control-label">Cantidad</label>
               <div class="col-sm-2">
                 <div class="input-group">
                      <select class="form-control" id="selectC" name="cantidad">
                      	
                      </select>                  
                  </div>
                </div>

                </br>
                <div id="IdPedidoDiv">
                <input type="hidden" name="idpedido" id='id_pedido' value="<?=$idPedido;?>" >
                 <input type="hidden" name="idCliente" id='id_cliente' value="<?=$idCliente;?>" >
                  <input type="hidden" id='client_level' value="<?=$client_level;?>" >
                </div>

        	  <div class="col-sm-2">      
              <div class=" col-sm-1">
                <input type="submit" class="btn btn-primary" name="HacerPedido" id="PedidoOrder" value="Agregar "/> 
             </div>         
          </div>
         </div>   
               
           
      
     </form>                
<!---->
      </div>
    </div>
  </div> 
  <!-- prueba my bitch -->
      
          <div class="col-md-4" id="notas">
                 <label for='notas'>Notas del pedido:</label>
                 <textarea class="form-control" rows="7"  id="TextNotas" name="textA" placeholder="Escriba aqui cualquier comentario adicional que quiera agragar junto a su pedido..."></textarea>
                 </br>
                 <div id="terminarPedido"></div>
                 </br>
          </div>
       
</div>
</div>
