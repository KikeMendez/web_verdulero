/********************************************************
  |	ALL THE API URLS  									|
  *******************************************************
*/

// LOGIN check login
/***************************************************************************************************/
	var check_login = "http://www.api.verdulero.com/index.php/api/cliente/check_user";
	// route controller Cliente method set session
	var set_session = "Cliente/set_session";
/***************************************************************************************************/	

// ADMIN 
/***************************************************************************************************/
	// display orders and details.			
	var misPedidos = "http://www.api.verdulero.com/index.php/api/Pedidos/mis_pedidos";
	
	var mis_pedidos_detalle = "http://www.api.verdulero.com/index.php/api/Pedidos/mis_pedidos_detalle?id_pedido=";

	// Add a product & add a client.
    var addProduct = "http://www.api.verdulero.com/index.php/api/Productos/agregar_producto";

    var addClient  = "http://www.api.verdulero.com/index.php/api/Cliente/agregar";
/***************************************************************************************************/


// PEDIDO
/***************************************************************************************************/
	// Insert a new order
	var new_order = "http://www.api.verdulero.com/index.php/api/pedidos/nuevo_pedido?id=";
	// Route controller Pedido set_session
	var session_id_pedido = "Pedido/session_id_pedido";
	// Insert a new item in pedido_fila
	var insert_order_item = "http://www.api.verdulero.com/index.php/api/pedidos/pedido_fila_agregar";
	// display client order 
	var display_order = "http://www.api.verdulero.com/index.php/api/Pedidos/mostrar_fila?id_pedido=";
	// delete order item
	var delete_order_item = "http://www.api.verdulero.com/index.php/api/Pedidos/borrar_item?id=";
/***************************************************************************************************/

// PRODUCTO
/***************************************************************************************************/
	// list all the products
	var list_product = "http://www.api.verdulero.com/index.php/api/Productos";
	// get the posible units for a single  product
	var posible_units = "http://api.verdulero.com/index.php/api/productos/get_unidad?item=";
/***************************************************************************************************/
