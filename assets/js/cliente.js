$(document).ready(function (){

// FUNCTION TO CHECK VALID EMAIL	
 $.validator.methods.email = function( value, element ) {
  return this.optional( element ) || /^\b[A-Z0-9._%-]+@[A-Z0-9.-]+\.[A-Z]{3,4}\b$/i.test( value );
}	
 /*
 * VALIDATION DEFAULTS 	
*/
  $.validator.setDefaults({
    errorClass: 'help-block',
    highlight: function(element) {
      $(element)
        .closest('.form-group')
        .addClass('has-error');
    },
    unhighlight: function(element) {
      $(element)
        .closest('.form-group')
        .removeClass('has-error');
    },
    errorPlacement: function (error, element) {
      if (element.prop('type') === 'text' || element.prop('type') === 'password' ) {
        error.insertAfter(element.parent());
      } else {
        error.insertAfter(element);
      }
    }
  }), // end validator defaults

  	 /*
	 * VALIDATE THE FORM.
  	*/
	$("#frmLogin").validate({			
		rules:{
			shop:{required:true},
			password:{required:true, minlength: 6}
		},

		messages: {
			shop: {
				required: "Por favor ingrese el nombre de su local.",
				
			},

			password: {
				required: "Por favor ingrese su password.",
				minlength: "El password debe contener al menos 6 caracteres"
			}
			
		}

	});	
 /*
 * FORM SUBMIT REQUEST API FOR A SINGLE USER INFORMATION.	
*/	
	$("#frmLogin").submit(function(evt){
			evt.preventDefault();
			
			var shop = $("#shop").val(); // get the value from the form.
			// encrypt password to md5.
			var md5 = $().crypt({method:"md5",source:$("#password").val()});
					
			$.ajax({
				url:  check_login,
				type: $("#frmLogin").attr('method'),
				data: { shop: shop,password:md5},
				cache :  false ,
				// IF THERE WAS NOT ANY ERROR
				// STORE THE USER INFORMATION.
				success: function(data){
					var id 				= 'id='+data[0].id;
					var name 			= '&name='+data[0].nombre;
					var mail 			= '&mail='+data[0].mail;
					var telefono 		= '&phone='+data[0].telefono;
					var apiKey   		= '&apiKey='+data[0].api_key;
					var nivel_listados	= '&nivel_listado='+data[0].nivel_listados;
					var nivel = data[0].nivel_listados;
					// ALL USER DATA.
					var user_data = id + name + mail + telefono + apiKey + nivel_listados;
					
					// THIS AJAX SENDS THE USER INFORMATION 
					//TO A CLIENTE CONTROLLER SET_SESSION METHOD
					$.ajax({
						type: 'post',
						url: set_session,
						data: user_data, // all user data
						cache: false,
						success: function(){
							// CHECK CLIENT LEVEL BEFORE REDIRECTING
							// ADMIN IS LEVEL 4  
							if (nivel == 4) {								
								window.location.replace("Admin");	
							}else{
								window.location.replace("Pedido");
							}
						}
					});// end of ajax session
						
				}// end success

			});	
	});// end submit function
});

					
					
					
					 
					

