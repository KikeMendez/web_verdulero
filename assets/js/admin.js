$(document).ready(function(){
/*AGREGAR PRODUCTO DIV*/
	$("#AgregarProducto").click(function (event){
		event.preventDefault();
		$("#SubTitle").text("Agregar Producto");
		$("#Producto").show();
		$("#Detalle").html('');
		$("#Cliente").hide();
		//hide the welcome div
		$("#Welcome").hide();
		$("#Pedido").hide();
		$("#nombre_producto").focus();
		// hide the panel admin title
		$("#adminPanel").hide();


	});// end of AgregarProducto div container.
/*
 * VALIDATION DEFAULTS 	
 * The alpha method validate the input. (Only letter and spaces) 
 */
	$.validator.addMethod("alpha", function(value, element) {
    	return this.optional(element) || value == value.match(/^[a-zA-Z\s]+$/);
 	});

  $.validator.setDefaults({
    errorClass: 'help-block',
    highlight: function(element) {
      $(element)
        .closest('.form-group')
        .addClass('has-error');
    },
    unhighlight: function(element) {
      $(element)
        .closest('.form-group')
        .removeClass('has-error');
    },
    // 
    errorPlacement: function (error, element) {
      if (element.prop('type') === 'text' || element.prop('class') === 'form-control' ) {
        error.insertAfter(element.parent());
      } else {
        error.insertAfter(element);
      }
    }
  });

	// VALIDATION PRODUCTO // SELECT THE FORM PRODUCTO
	$("#frmAddProduct").validate({
		rules: {
			ProductoNombre: {
			 required : true,
			 alpha: true 
			} ,
			unidades: {
				required: true
			},
			estado: {
				required: true
			},
			nivel_listado: {
				required: true
			}
		},
		messages:{
			ProductoNombre: {
				required: "Ingrese nombre del producto",
				alpha: "Solo letras y espacios son permitidos"
			}, 
			unidades: "Ingrese tipo de unidades disponibles",
			estado: "Ingrese estado del producto",
			nivel_listado: "Ingrese el nivel de listado de este producto"
		}
	}); // END OF VALIDATION

	$("#frmAddProduct").submit(function (event){
				event.preventDefault();
				var dataForm = $(this).serialize();	// serialize the form.		
				// ajax to insert a new product
				$.ajax({
					url: addProduct, 
					data: dataForm,
					type: $(this).attr('method'), // METHOD FROM FORM
					success: function(){
						$("#nombre_producto").focus();
						$("#message").text('Producto insertado correctamente').fadeIn(2000, function(){
							$(this).fadeOut(3000);
						});
					} 
				}); // END OF AJAX INSERT PRODUCT
				
				// CLEAN THE FORM AFTER INSERT
				$("#frmAddProduct")[0].reset();
			});	// END OF SUBMIT

/***************************************AGREGAR CLIENTE********************************************************/	
	
	/*AGREGAR CLIENTE DIV*/
	$("#AgregarCliente").click(function (event){
		event.preventDefault();
		$("#SubTitle").text('Agregar Cliente');
		$("#Producto").hide();
		$("#Detalle").html('');
		//hide the welcome div
		$("#Welcome").hide();
		$("#Cliente").show();
		$("#Pedido").hide();
		$("#email").focus();
		// hide the title "Panel de administracion"
		$("#adminPanel").hide();
	});

	
 // VALIDATION DEFAULTS 	
 // The alpha method validate the input. (Only letter and spaces) 
 
	$.validator.addMethod("alpha", function(value, element) {
    	return this.optional(element) || value == value.match(/^\S+[a-zA-Z\s]+$/);
 	});

	// FUNCTION TO CHECK VALID EMAIL	
 $.validator.methods.email = function( value, element ) {
  return this.optional( element ) || /^\b[A-Z0-9._%-]+@[A-Z0-9.-]+\.[A-Z]{3,4}\b$/i.test( value );
}
	// Validate the form
	$("#AddClient").validate({
		// add rules
		rules: {
			mail: {
				required: true,
				email: true
			},
			nombre: {
				required: true,
				alpha: true,
				minlength: 3
			},
			telefono: {
				required: true,
				minlength: 8
			},
			password: {
				required: true,
				minlength: 6
			},
			nivel: {
				required: true
			}
		},
		//Set messages errors
		messages: {
			mail:{
				required: "Ingrese el email de su cliente",
				email : "Por favor ingrese un cuenta de correo valida"
			}, 
			nombre:{
				required: "Ingrese nombre del local de su cliente",
				alpha : "Solo letras son permitidos en este campo </br> Los espacios al principio no son permitidos",
				minlength: "Minimo 3 letras"
			}, 
			telefono:{
				required: "Ingrese el telefono de su cliente",
				minlength: "Este campo debe contener al menos 8 digitos"
			}, 
			password: {
				required: "Ingrese el password para su cliente",
				minlength: "Minimo 6 caracteres"
			},
			nivel: "Ingrese el privilegio para su cliente"
		}

	}); // END OF FORM CLIENTE VALIDATION
	
	// 	REQUEST TO ADD A NEW CLIENT
	$("#AddClient").submit(function (event){
		event.preventDefault();
		var dataForm = $(this).serialize();	// serialize the form.		
		// ajax to insert a new product
		$.ajax({
			url: addClient, // URL FROM FORM
			data: dataForm,
			type: $(this).attr('method'), // METHOD FROM FORM
		
			success: function(){
				$("#email").focus();
				$("#message").text('Cliente agregado correctamente').fadeIn(2000, function(){
					$(this).fadeOut(3000);
				});
			}
		}); // END OF AJAX INSERT PRODUCT
		
		// CLEAN THE FORM AFTER INSERT
		$("#AddClient")[0].reset();
	});	// END OF SUBMIT



/*******************************-MOSTRAR PEDIDOS-***************************************************/
//When the link is clicked the orders will be display it.
$("#MostrarPedido").click(function (event){
		event.preventDefault();
		$("#Cliente").hide();
		$("#Producto").hide();
		$("#Detalle").html('');
		$("#SubTitle").text('Mis Pedidos');
		$("#Pedido").show();
		// hide the title "Panel de administracion"
		$("#adminPanel").hide();
		$("#Welcome").hide();

		// store the table html
		 
		var html ="<div class='table-responsive'  style='margin:auto;'> ";
			html +="<table class='table table-striped'>";
			html +="<thead><tr><th>Pedido</th><th>Nombre</th><th>Fecha entrega</th>";
			html +="<th>Notas de pedido</th><th></th><th>Ver</th></tr></thead>";
			html +="<tbody id='displayOrder'></tbody></table></div>"; 
		// store the order's rows
		var ap;
		$.ajax({
			url: misPedidos, // from variable at the top of the this page
			type:'get',
			success: function(response){
				// display the table
				$("#Pedido").html(html);
				$.each(response, function(index,order){
					ap += "<tr>";
					ap += "<td>"+order.Pedido+"</td>";
					ap += "<td>"+order.Cliente+"</td>";
					ap += "<td>"+order.Entrega+"</td>";
					ap += "<td>"+order.notas+"</td>";
					ap += "<td></td>";
					ap += "<td><button type='button' id='verDetalle' class='btn btn-info' value='"+order.Pedido+"'>Detalle</button></td>";
					ap += "</tr>";
				});// end of each
				// DISPLAY THE ORDERS	
				$("#displayOrder").append(ap);

				// DISPLAY DETAILS OF EACH ORDER. SERCH BY ORDER_ID.
				$("#displayOrder").on("click","button", function(){
					// value of order taken from button
					var order_id = $(this).val(); 
					// table  to display detail of order
					var table = "<div class='table-responsive'  style='margin:auto;'>";
						table +="<table class='table table-striped'>";
						table +="<thead><tr><th>Producto</th><th>Cantidad</th><th>Unidad</th>";
						table +="</tr></thead>";
						table +="<tbody id='displayOrderDetail'></tbody></table></div>"; 
					var rows;	
					
				
					// request for an order detail
					$.ajax({
						url: mis_pedidos_detalle + order_id, 
						type:'get',	
						success: function(response){
							$("#Pedido").hide()
							//Cliente info.
							$("#SubTitle").html("<div class='row'><div class='col-sm-4'>Cliente: "
								+response[0].Cliente  +"</div>"+"<div class='col-sm-4'>Telefono: "
								+response[0].Telefono +"</div>"+"<div class='cols-sm-4'>Email: "
								+response[0].Email+"</div></div>");
							//show the table
							$("#Detalle").html(table);
				
							$.each(response, function(index,order){								 
								rows += "<tr>";
								rows += "<td>"+order.Producto+"</td>";
								rows += "<td>"+order.Cantidad+"</td>";
								rows += "<td>"+order.Unidad+"</td>";
								rows += "</tr>";

							});// end of each
							
							//Button to print the order
							var print_order_button = "</br><button class='btn btn-success' id='print_order'>Imprimir pedido</button>";

							// Display the details of each clicked order and the button.
							$("#displayOrderDetail").append(rows);
							$("#displayOrderDetail").append(print_order_button);	
							
							// Function to print the order
							$("#print_order").click(function (){
								window.print();
							
							});	// end of print order						
						}//end of success
						
					});// end of ajax display order detail

				}); // end of displayOrder.on
					
			}, // end of success
			error: function(){
				$("#SubTitle").text("Usted no ha recibido ningun pedido para hoy!");				
			} // end of error
		}); // end of ajax mis_pedidos

	}); // end of mostraPedido.click

});// end of document.ready

			          
			  
			   