$(document).ready(function(){
	$("#notas").hide();
	$("#fecha").hide();
//*****************************************************************************************
// id_cliente STORED A SESSION ID VALUE BROUGTH FROM A HIDDEN FIELD IN THE PEDIDOS FORM
//*****************************************************************************************
	var id_cliente = $("#id_cliente").val(); 
// LEVEL OF CLIENT USED TO DISPLAY PRODUCT BROUGTH FROM A HIDDEN FIELD IN THE PEDIDOS FORM.
	var client_level = $("#client_level").val(); 
//*********************************************************************************************
//	 idPedido STORED A SESSION ID_PEDIDO VALUE BROUGTH FROM A HIDDEN FIELD IN THE PEDIDOS FORM
//*********************************************************************************************
	var idPedido = $("#id_pedido").val();

//*********************************************************************************************
//	THIS IF CHECKS IF NOT EXISTS PEDIDO ID.
//	IN THAT CASE WILL CREATE A NEW ORDER.
//*********************************************************************************************
	if(idPedido == false)
	{
		//console.log("false");
		
		$("#frmgroup").hide();    // hide the form 
		$("#tableOrder").hide(); // hide table orders
		
		
		// BUTTON NUEVO PEDIDO
		$("#btnNewOrder").html("<div class=''><button type='button' value='nuevo pedido' class='btn btn-primary'><span class='glyphicon glyphicon-plus'></span> Nuevo Pedido</button></div>");	
		$("#mensaje").text('Nuevo pedido');
		//$("#notas").show();
		$("#fecha").show();
		
		// THIS FUNCTION SEND THE ID_CLIENTE AND NOTAS TO GENERATE A NEW ORDER
		$("#btnNewOrder").click(function(){			
			// NOTAS HAS THE VALUE OF TEXTAREA
			var fechaEntrega = $("#fechaEntrega").val();
			alert(fechaEntrega);
			var notas = $.trim($("#TextNotas").val());
			// CREATE A NEW ORDER
			$.ajax({
				url: new_order + id_cliente +'&notas='+notas,
				dataType: 'jsonp',
				type:'get',
				cache: false,
				success: function(response){
				/******************************************************************************************
				 *| IMPORTANT: This idPedido stored the last id inserted in pedido table. DO NOT REMOVE IT!|
				 ******************************************************************************************
				 */	  idPedido = 'id_pedido='+response.id_pedido; 				
				/******************************************************************************************
				*/	
					$.ajax({
							type: 'post',
							url:  session_id_pedido,
							data: idPedido,
							cache: false,
							success: function(){
							// PASS THE idPedido's VALUE TO THE FIELD IN THE FORM 
								$("#id_pedido").attr("value",response.id_pedido);
								$("#fecha").hide(); // hide the datepicker
								$("#notas").show();
								$("#btnNewOrder").hide(); // hide the nuevo pedido button
								$("#mensaje").text('Agregar producto');
								$("#frmgroup").show();								
							} // end success calback
					});// end ajax
				 												
				} // end success click callback
			});// end ajax insert new pedido
		}); // end click function
	 
	}else{
		$("#displayOrder").show();
	}// end if


	/***************************************************
	 * THIS FUNCTION SEARCH IN THE API FOR ALL PRODUCTS
	 ***************************************************
	 */
	function selectProduct(){
		//GET A REFERENCE TO THE <SELECT> ELEMENT 
		$select = $('#selectP'); // <select> producto

		$unidad = $('#selectU'); // <select> unidad

		$cantidad = $('#selectC'); // <select> cantidad
		
		// AJAX REQUEST TO BRING ALL PRODUCT'S INFORMATION

		$.ajax({
			url: list_product,
			dataType: 'jsonp',
			data:{level: client_level},
			type: 	  'get',
			cache :  false ,
			success: function(response){
				
			//CLEAR THE CURRENT CONTENT OF THE SELECT
    		$select.html('');
    		$select.append('<option value="">Elija</option>');
			$.each(response, function (index,producto){ // loopping the response
			
			/********************************************	
			 *  DISPLAY PRODUCTS
			 *	IF THE PRODUCTS ARE AVAILABLE TO ORDER IT. THEY WILL FILL THE SELECT OPTIONS.
			 */
				if (producto.es_activo == true){
					$select.append('<option value="'+producto.id+'">'+producto.nombre+'</option>');
				}

			});// end of each loop

			/*****************************************************************************************
			 * LOADS THE "UNIDADES_POSIBLES" VALUE FOR EACH PRODUCT. 
			 * WHEN A PRODUCT IS SELECTED, THE UNIDAD'S SELECT BOX WILL BE FILLED WITH THE VALUE OF IT.			
			 ******************************************************************************************
			 */
			$select.change(function(){
					var item = $select.val(); //item has the product name from the select product.
					$.ajax({ 
						url: posible_units + item,
						dataType: 'jsonp',
						type: 	  'get',
						cache :  false ,
						success: function(response){
							$unidad.html('');
							$unidad.append('<option value="">Elija</option>');
							/*
							* Check the posible values
							// str has unidades posibles from the api
							*/
							var str = response[0].unidades_posibles; 
							// split into an array
							var str_splitted = str.split(",");

							// loop to display the values
							for (var i = 0; i < str_splitted.length; i++) {
								$unidad.append('<option value="' + str_splitted[i] + '">' + str_splitted[i] + '</option>');
								
							}
							
						} // end of success function
					});//end of the second ajax
							
				});// end of change			
				$unidad.html(''); // cleaning the unidad select.
							
			} // end of success function
		});//end of first ajax request
					
	} // END OF SELECTPRODUCT FUNCTION

	/******************************************************************************** 	
	 *  THIS FUNCTION CANTIDAD LOADS THE CANTIDAD SELECT BOX AND FILLED WITH VALUES
	 *	FROM 1 TO 50 WHEN A TYPE OF UNIDAD IS SELECTED.
	 ********************************************************************************
	 */
		function cantidad(){
			$unidad.change(function(){
				$cantidad.append('<option value="">Elija</option>');
				for (var i = 1; i <= 50; i++) {
					$cantidad.append('<option value="' + i + '">' + i + '</option>');
				};
			}); // end of unidad.change
			$cantidad.html('');
		 } // en of cantidad function

		/************************************************
	     * CALLING THE FUNCTIONS TO DISPLAY THE SELECTORS
		 ************************************************
		 */ 
		selectProduct();
		cantidad();	
		
		/******************************************************
		 **	METHOD FOR PEDIDO SUBMIT WHEN THE FORM IS SUBMITED
		 ******************************************************
		 */
		
		$('#frmPedidos').submit(function(evt){
			// STORE THE SELECT'S VALUES FROM THE FORM IN VARIABLES.
				evt.preventDefault();
				var u = $unidad.val();
				var c = $cantidad.val();
				var p = $select.val();	
							
			// CHECKS IF THERE IS AN EMPTY VALUE
			if (p == false)
			{
				alert("Por favor seleccione un producto.");
				$select.focus();
			}else if(u == false)
			{
				alert("Por favor seleccione un tipo de unidad.");
				$unidad.focus();
			}else if(c == false)
			{
				alert("Por favor seleccione la cantidad que necesita.");
				$cantidad.focus();
			}else
			{	
				// HERE IS WHERE I HAVE TO MAKE IT BETTER!!!!!			
				// SERIALIZE DATA FROM THE FORM PEDIDO
				var dataform = $(this).serialize();
					
				// SEND TO API TO SAVE INTO PEDIDO_FILA
				$.ajax({
					url: insert_order_item,
					type: $("#frmPedidos").attr('method'),
					data: dataform,
					cache: false,
					success: function(response){
						// VARIABLE TO STORED THE ORDER INFORMATION.
						var ap;
						// VARIABLE TO STORED THE ID_PEDIDO.
						var id_pedi = response; 
						
						// SEND REQUEST TO API MOSTRAR_FILA PASSING idPedido AS PARAMETER.;	
						$.ajax({
							url: display_order + id_pedi,
							type:'get',
							success: function(response){
								// SHOW ALL ORDERS TAKEN
								$("#tableOrder").show();

								// each method to display one by one
								$.each(response, function (index,producto){	 
								$("#displayOrder").html(''); // Clean the table's order								
									ap += "<tr>";
									ap += "<td>"+producto.producto_nombre+"</td>";
									ap += "<td>"+producto.cantidad+"</td>";
									ap += "<td>"+producto.unidad+"</td>";
									ap += "<td>"+producto.fecha_entrega+"</td>";
									ap += "<td></td>";
									ap += "<td><button type='button' id='borrar' class='btn btn-danger' value='"+producto.id+"'>Borrar</button></td>";
									ap += "</tr>";
								}); // en of each to display product
								
								// DISPLAY THE ORDER	
								$("#displayOrder").append(ap);
								
								// FUNCTION TO DELETE AN ORDER. (using event delegation on buttons)
								$("#displayOrder").on("click","button", function(){
									// ID OF EACH BUTTON ORDER 
									var id_order = $(this).val();
									
									// CLEAN THE DELETED ITEM
									var clean = $(this).closest('tr').remove();									
										
										// SEND TO API TO DELETE
										$.ajax({
											url: delete_order_item + id_order,	
											dataType: 'jsonp',
											type: 'get',
											success: function(){												
												// CLEAN THE DELETED ITEM
												clean;												
											}
										});// end of ajax delete

										// IF THE DELETED ITEM WAS THE LAST ONE THE TABLE WILL HIDE.
										var order_length = $("#displayOrder tr").length;
										// CHECK
										if(order_length == 0){
											$("#tableOrder").hide();
										}									
									
								}); // end of event delegation
																			
							} // end of success display order's
						
						});// end of mostrar ajax
						
						// ANIMATION FOR THE MENSAJE
						$('#mensaje').text('Producto agregado corectamente').fadeOut(2000, function(){
							$("#mensaje").text('Agregar otro producto').fadeIn(1000);});	
						$("#terminarPedido").html("<button class='btn btn-success'>Terminar pedido</button>");
						selectProduct(); // load the product selector again
						cantidad(); // load the cantidad selector again
						
					} // end of ajax send to api response.
						
				}); // end of ajax send to api
												
			}// END OF ELSE. CHECKS IF THERE IS AN EMPTY VALUE

		}); // end form submit

});//end of ready

		 
